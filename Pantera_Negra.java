package FATEC2018;
import robocode.*;
import java.awt.Color;

/**
 * Pantera_Negra - a robot by Matheus Mendes
 */
	public class Pantera_Negra extends AdvancedRobot
{
 
	public void run() {
		// Initialization of the robot should be put here

		// After trying out your robot, try uncommenting the import at the top,
		// and the next line:

		setBodyColor(Color.black);			//body > corpo
		setGunColor(Color.gray);			//gun > arma
		setRadarColor(Color.red);			//radar > radar
		setScanColor(Color.red);			//scan > varredura

		// Robot main loop
			while(true) 
		{
				for(int dir=0;dir<4;dir++)
				{
					ahead(100);
					turnRight(90);
					turnGunRight(360);	
					
				}
				for(int esq=4;esq>0;esq--)
				{
					ahead(100);
					turnLeft(90);
					turnGunRight(360);						
				}		
		}
	}	
	
	
	public void onScannedRobot(ScannedRobotEvent e) {
		// Replace the next line with any behavior you would like
	
				if (e.getDistance() < 200 ) 
				{
					fire(3);
				} else {
				    fire(1);
					   }

				if(getEnergy() == 65)
			{
				for(int dir=0;dir<4;dir++)
				{
					setAhead(100);
					setTurnRight(90);
					setTurnGunRight(360);	
					execute();	
				}
				for(int esq=4;esq>0;esq--)
				{
					setAhead(100);
					setTurnRight(90);
					setTurnGunRight(360);	
					execute();	
				}
			} else if (getEnergy() < 50) 
			{
				for(int dir45=0;dir45<4;dir45++)
				{
					setBack(100);
					setTurnLeft(90);
					setTurnGunRight(360);	
					execute();		
				}
				for(int esq45=4;esq45>0;esq45--)
				{
					setBack(100);
					setTurnLeft(90);
					setTurnGunRight(360);	
					execute();	
				}
			}
				
	}
	public void onHitByBullet(HitByBulletEvent e) {
	// Replace the next line with any behavior you would like
		back(200);
	}

//	public void onScannedWall(ScannedWallEvent e) {
			
	//	back(200);
	//}
}